#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#define COUNT_LIMIT 20000
#define STR_LIMIT 50
#define SPACE 32

int cond(char *word1, char *word2) {
	int word1_length = strlen(word1);
	int word2_length = strlen(word2);
	
	if (word1_length > word2_length) return 0;
	if (!strcmp(word1, word2)) return 0;
	if (word1_length == word2_length) {
		for (int i=0; i < word1_length; i++) {
			if (word1[i] > word2[i]) {
				return 0;
			} else if (word1[i] < word2[i]) {
				return 1;
			}
		}
	}
	return 1;
}

void quicksort(char **arr, int left, int right) {
	int i=left, j=right;
	char *pivot = arr[(left + right) / 2];
	char *temp;

	do {
		while (cond(arr[i], pivot)) i++;
		while (cond(pivot, arr[j])) j--;

		if (i <= j) {
			temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
			i++;
			j--;
		}
	} while (i <= j);

	if (left < j) {
		quicksort(arr, left, j);
	}

	if (i < right) {
		quicksort(arr, i, right);
	}
}

int check(char *str) {
	int length = strlen(str);

	str[length-1] = '\0';

	for (int i=0; i < length; i++) {
		if (str[i] == SPACE) {
			printf ("Space is not allowed input again\n");
			return 0;
		}
	}
	return 1;
}

int main() {
	char **arr;
	int word_count;

	scanf("%d", &word_count);
	while (
		!(word_count <= COUNT_LIMIT &&
		word_count >= 0)
	) {
		printf ("Word count is from 0 to 20,000\n");
		scanf("%d", &word_count);
	}
	if (word_count == 0) return 0;
	rewind(stdin);

	arr = malloc(sizeof(char *) * word_count);
	for (int i=0; i < word_count; i++) {
		arr[i] = (char *)malloc(sizeof(char) * STR_LIMIT);
		fgets(arr[i], sizeof(char) * STR_LIMIT, stdin);
		while (!check(arr[i])) {
			memset(arr[i], 0, sizeof(char) * STR_LIMIT);
			fgets(arr[i], sizeof(char) * STR_LIMIT, stdin);
		}
	}

	quicksort(arr, 0, word_count - 1);
	
	printf ("============ output ===========\n");
	for (int i=0; i < word_count; i++) {
		if (i && !strcmp(arr[i-1], arr[i])) continue;
		printf ("%s\n", arr[i]);
		free(arr[i]);
	}
	
	free(arr);
	return 0;
}
